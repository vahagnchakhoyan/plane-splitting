/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package plane.splitting;

import static java.lang.Math.*;
import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import javafx.scene.paint.Color;

class Artist {
    private class Point {
        int x; int y;
        Point(int x, int y) { this.x = x; this.y = y; }
    }
    
    public static Color lineColor = Color.BLACK;
    public static Color fillColor = Color.RED;
    public static Color areaHighlightColor = Color.LIGHTBLUE;
    public static Color vertexHighlightColor = Color.YELLOWGREEN;
    public static Color backgroundColor = Color.WHITE;
    public static double lineWidth = 1;
    
    private WritableImage canvas;
    private WritableImage rememberedSquare;
    
    private PixelWriter brush;
    private PixelReader eyes;
    
    Artist(WritableImage writableImage) {
        canvas = writableImage;
        brush = writableImage.getPixelWriter();
        eyes = writableImage.getPixelReader();
    }
    
    private void draw1PxLine(double x0, double y0, double length, double direction) {
        abstract class NextGetter {
            abstract double getFor(double coordinate);
        }
        
        final char onX = 'x';
        final char onY = 'y';
        final char onBoth = 'b';
        
        double xStep = cos(toRadians(direction));
        double yStep = sin(toRadians(direction));
        
        double xDirection = xStep < 0 ? -1 : 1;
        double yDirection = yStep < 0 ? -1 : 1;
        
        double yCurrent;
        double xCurrent;
                
        double passed = 0;
        char state;
        
        NextGetter xNext = xDirection == 1 ? new NextGetter() {
            @Override
            double getFor(double coordinate) {
                return floor(coordinate + 1);
            }
        } : new NextGetter() {
            @Override
            double getFor(double coordinate) {
                return ceil(coordinate - 1);
            }
        };
        NextGetter yNext = yDirection == 1 ? new NextGetter() {
            @Override
            double getFor(double coordinate) {
                return floor(coordinate + 1);
            }
        } : new NextGetter() {
            @Override
            double getFor(double coordinate) {
                return ceil(coordinate - 1);
            }
        };
        
        if(x0%1 == 0 && y0%1 != 0) {xCurrent = x0; yCurrent = y0; state = onX;}
        else if(x0%1 != 0 && y0%1 == 0) {xCurrent = x0; yCurrent = y0; state = onY;}
        else if(x0%1 == 0 && y0%1 == 0) {xCurrent = x0; yCurrent = y0; state = onBoth;}
        else {
            brush.setColor((int)x0, (int)y0, lineColor);
            
            double tillWholeX = xNext.getFor(x0) - x0;
            double tillWholeY = yNext.getFor(y0) - y0;
            
            if(tillWholeX/xStep > tillWholeY/yStep) {
                passed = tillWholeY/yStep;
                xCurrent = x0 + tillWholeY * xStep / yStep;
                yCurrent = yNext.getFor(y0);
                state = onY;
            } else if(tillWholeX/xStep < tillWholeY/yStep) {
                passed = tillWholeX/xStep;
                xCurrent = xNext.getFor(x0);
                yCurrent = y0 + tillWholeX * yStep / xStep;
                state = onX;
            } else {
                passed = tillWholeY/yStep;
                xCurrent = xNext.getFor(x0);
                yCurrent = yNext.getFor(y0);
                state = onBoth;
            }
        }
        
        double tillWholeX;
        double tillWholeY;

        while(passed < length) {
            tillWholeX = xNext.getFor(xCurrent) - xCurrent;
            tillWholeY = yNext.getFor(yCurrent) - yCurrent;
            
            switch(state) {
                case onX:
                    brush.setColor((int)min(xCurrent, xNext.getFor(xCurrent)), (int)yCurrent, lineColor);
                    break;
                case onY:
                    brush.setColor((int)xCurrent, (int)min(yCurrent, yNext.getFor(yCurrent)), lineColor);
                    break;
                case onBoth:
                    brush.setColor((int)min(xCurrent, xNext.getFor(xCurrent)), (int)min(yCurrent, yNext.getFor(yCurrent)), lineColor);
                    break;
            }
            
            if(tillWholeX/xStep > tillWholeY/yStep) {
                passed += tillWholeY/yStep;
                xCurrent += tillWholeY * xStep / yStep;
                yCurrent = yNext.getFor(yCurrent);
                state = onY;
            } else if(tillWholeX/xStep < tillWholeY/yStep) {
                passed += tillWholeX/xStep;
                xCurrent = xNext.getFor(xCurrent);
                yCurrent += tillWholeX * yStep / xStep;
                state = onX;
            } else {
                passed += tillWholeY/yStep;
                xCurrent = xNext.getFor(xCurrent);
                yCurrent = yNext.getFor(yCurrent);
                state = onBoth;
            }
        }
    }
    
    public Color getColor(int x, int y) {
        return eyes.getColor(x, y);
    }
    
    public void fillArea(int x0, int y0, Color color) {
        Color targetColor = eyes.getColor(x0, y0);
        
        int canvasHeight = (int) canvas.getHeight();
        int canvasWidth = (int) canvas.getWidth();
        
        boolean[][] checkedMatrix = new boolean[canvasHeight][canvasWidth];
        for(int i = 0; i < canvasHeight; ++i) {
            Arrays.fill(checkedMatrix[i], false);
        }
        
        Queue<Point> queue = new LinkedList<>();
        
        Point tempPoint;
        queue.add(new Point(x0, y0));
        while(!queue.isEmpty()) {
            tempPoint = queue.remove();
            checkedMatrix[tempPoint.x][tempPoint.y] = true;
            
            if(!eyes.getColor(tempPoint.x, tempPoint.y).equals(targetColor)) continue;
            
            brush.setColor(tempPoint.x, tempPoint.y, color);
            
            if(tempPoint.x + 1 < canvasHeight && !checkedMatrix[tempPoint.x + 1][tempPoint.y]) {queue.add(new Point(tempPoint.x + 1, tempPoint.y));}
            if(tempPoint.x - 1 >= 0 && !checkedMatrix[tempPoint.x - 1][tempPoint.y]) {queue.add(new Point(tempPoint.x - 1, tempPoint.y));}
            if(tempPoint.y + 1 < canvasWidth && !checkedMatrix[tempPoint.x][tempPoint.y + 1]) {queue.add(new Point(tempPoint.x, tempPoint.y + 1));}
            if(tempPoint.y - 1 >= 0 && !checkedMatrix[tempPoint.x][tempPoint.y - 1]) {queue.add(new Point(tempPoint.x, tempPoint.y - 1));}
        }
    }
    
    public void fillCircle(double centerX, double centerY, double radius) {
        int yToUp;
        int yToDown;
        int xToLeft;
        int xToRight;
        
        if(centerX % 1 > 0) {
            yToUp = (int)centerY;
            while(centerY - yToUp < radius) {
                brush.setColor((int)centerX, yToUp - 1, fillColor);
                --yToUp;
            }
            
            yToDown = (int)centerY;
            while(yToDown - centerY < radius) {
                brush.setColor((int)centerX, yToDown, fillColor);
                ++yToDown;
            }            
        }
        if(centerY % 1 > 0) {
            xToLeft = (int)centerX;
            while(centerX - xToLeft < radius) {
                brush.setColor(xToLeft - 1, (int)centerY, fillColor);
                --xToLeft;
            }
            
            xToRight = (int)centerX;
            while(xToRight - centerX < radius) {
                brush.setColor(xToRight, (int)centerY, fillColor);
                ++xToRight;
            }
        }
        
        yToUp = (int)floor(centerY);
        while(centerY - yToUp < radius) {
            xToLeft = (int)floor(centerX);
            while(sqrt((centerX - xToLeft) * (centerX - xToLeft) + (centerY - yToUp) * (centerY - yToUp)) < radius) {
                brush.setColor(xToLeft - 1, yToUp - 1, fillColor);
                --xToLeft;
            }
            
            xToRight = (int)ceil(centerX);
            while(sqrt((xToRight - centerX) * (xToRight - centerX) + (centerY - yToUp) * (centerY - yToUp)) < radius) {
                brush.setColor(xToRight, yToUp - 1, fillColor);
                ++xToRight;
            }
            
            --yToUp;
        }
        
        yToDown = (int)ceil(centerY);
        while(yToDown - centerY < radius) {
            xToLeft = (int)floor(centerX);
            while(sqrt((centerX - xToLeft) * (centerX - xToLeft) + (yToDown - centerY) * (yToDown - centerY)) < radius) {
                brush.setColor(xToLeft - 1, yToDown, fillColor);
                --xToLeft;
            }
            
            xToRight = (int)ceil(centerX);
            while(sqrt((centerX - xToRight) * (centerX - xToRight) + (yToDown - centerY) * (yToDown - centerY)) < radius) {
                brush.setColor(xToRight, yToDown, fillColor);
                ++xToRight;
            }
            
            ++yToDown;
        }
    }
    
    public void cleanCanvas() {
        double height = canvas.getHeight();
        double width = canvas.getWidth();
        
        for(int y = 0; y < height; y++) { 
            for(int x = 0; x < width; x++) { 
                brush.setColor(x, y, backgroundColor);
            }
        }
    }
    
    public void drawSquare(double centerX, double centerY, double sideLength, double direction) {
        double x0 = centerX + cos(toRadians(direction - 135)) * sideLength * sqrt(2) / 2;
        double y0 = centerY + sin(toRadians(direction - 135)) * sideLength * sqrt(2) / 2;
        double currentDirection = direction;
        
        for(int i = 1; i <= 4; ++i, x0 += cos(toRadians(currentDirection)) * sideLength, y0 += sin(toRadians(currentDirection)) * sideLength, currentDirection += 90) {
            draw1PxLine(x0, y0, sideLength, currentDirection);
        }
    }
    
    public void rememberSquare(double centerX, double centerY, double sideLength) {
        int memoryWidth = (int)ceil(centerY + sideLength/2) - (int)floor(centerY - sideLength/2);
        int memoryHeight = (int)ceil(centerX + sideLength/2) - (int)floor(centerX - sideLength/2);
        rememberedSquare = new WritableImage(memoryWidth, memoryHeight);
        
        PixelWriter memoryWriter = rememberedSquare.getPixelWriter();
        
        WritablePixelFormat<IntBuffer> format = WritablePixelFormat.getIntArgbInstance();
        
        int[] buffer = new int[memoryWidth * memoryHeight];
        
        eyes.getPixels((int)floor(centerX - sideLength/2), (int)floor(centerY - sideLength/2), memoryWidth, memoryHeight, format, buffer, 0, memoryWidth);
	memoryWriter.setPixels(0, 0, memoryWidth, memoryHeight, format, buffer, 0, memoryWidth);
    }
    
    public void drawSquareFromMemory(double centerX, double centerY, double sideLength) {
        int memoryWidth = (int)ceil(centerY + sideLength/2) - (int)floor(centerY - sideLength/2);
        int memoryHeight = (int)ceil(centerX + sideLength/2) - (int)floor(centerX - sideLength/2);
        
        PixelReader memoryReader = rememberedSquare.getPixelReader();
        
        WritablePixelFormat<IntBuffer> format = WritablePixelFormat.getIntArgbInstance();
        
        int[] buffer = new int[memoryWidth * memoryHeight];
        
        memoryReader.getPixels(0, 0, memoryWidth, memoryHeight, format, buffer, 0, memoryWidth);
	brush.setPixels((int)floor(centerX - sideLength/2), (int)floor(centerY - sideLength/2), memoryWidth, memoryHeight, format, buffer, 0, memoryWidth);
    }
    
    public void forgetRememberedSquare() {
        rememberedSquare = null;
    }
}
