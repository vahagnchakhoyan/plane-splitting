/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package plane.splitting;

import static java.lang.Math.*;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PlaneSplittingController implements Initializable {
    private class Point {
        double x; double y;
        Point(double x, double y) { this.x = x; this.y = y; }
    }
    
    private final static double minDistanceToDrawVertexes = 10;
    private final static double vertexRadius = 2.5;
    private final static double highlightedVertexRadius = 4;
    
    public TextField countTextField = null;
    public TextField highlightNumberTextField = null;
    public ImageView imageView = null;
    public Button zoomOut = null;
    public Button zoomIn = null;
    public Label scaleLabel = null;
    
    private WritableImage writableImage;
    private Artist artist;
    
    private int canvasScale = 1;
    private final int canvasSideUnit = 500;
    
    private int count = 0;
    private int chosenVertex = 0;
    
    private DataManager dataManager = new DataManager();

    public ChangeListener countChangeListener = new ChangeListener<String>() {

        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
            String temp = newValue;
            
            temp = temp.replaceAll("[^\\d]", "");
            temp = temp.replaceFirst("^0+(?!$)", "");
            
            if(temp.length() > 3) temp = temp.substring(0, 3);
            if(temp.isEmpty()) temp = "0";
            
            if(!temp.equals(newValue)) countTextField.setText(temp);
                    
        }
    };
    
    public ChangeListener highlightNumberChangeListener = new ChangeListener<String>() {

        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
            String temp = newValue;
            
            temp = temp.replaceAll("[^\\d]", "");
            temp = temp.replaceFirst("^0+(?!$)", "");
            
            if(temp.isEmpty()) temp = "0";
            if(Integer.parseInt(temp) > 4*count*count) temp = Integer.toString(4*count*count);
            
            if(!temp.equals(newValue)) highlightNumberTextField.setText(temp);
        }
    };
    
    @FXML
    public void minusClicked(Event e) {
        if(count - 1 < 0) return;
        --count;
        
        countTextField.setText(String.valueOf(count));
        redrawSquares();
    }
    
    @FXML
    public void plusClicked(Event e) {
        if(count + 1 < 0 || count + 1 > 999) return;
        ++count;
        
        countTextField.setText(String.valueOf(count));
        dataManager.setSquareNumber(this.count);
        dataManager.calculationSteps(count);
        redrawSquares();
    }
    
    @FXML
    public void highlightClicked(Event e) {
        highlightNumberChanged(e);
    }
    
    @FXML
    public void zoomOut(Event e) {
        canvasScale /= 2;
        
        zoomIn.setDisable(false);
        if(canvasScale == 1) zoomOut.setDisable(true);
        
        scaleLabel.setText("x" + canvasScale);
        scaleNewCanvas();
        redrawSquares();
    }
    
    @FXML
    public void zoomIn(Event e) {
        canvasScale *= 2;
        
        zoomOut.setDisable(false);
        if(canvasScale == 4) zoomIn.setDisable(true);
        
        scaleLabel.setText("x" + canvasScale);
        
        scaleNewCanvas();
        redrawSquares();
    }
    
    private void scaleNewCanvas() {
        writableImage = new WritableImage(canvasScale * canvasSideUnit, canvasScale * canvasSideUnit);
        imageView.setImage(writableImage);
        
        artist = new Artist(writableImage);
    }
    
    @FXML
    public void algorythmClicked(Event e) throws Exception {
        Stage algorithmStage = new Stage();
        algorithmStage.setResizable(false);
        algorithmStage.setHeight(400);
        algorithmStage.setWidth(600);
        
        Parent root = FXMLLoader.load(getClass().getResource("algorithm/Algorithm.fxml"));
        
        Scene scene = new Scene(root);
        
        algorithmStage.setScene(scene);
        algorithmStage.initModality(Modality.APPLICATION_MODAL);
        algorithmStage.show();
    }
    
    @FXML
    public void aboutClicked(Event e) throws Exception {
        Stage aboutStage = new Stage();
        aboutStage.setResizable(false);
        aboutStage.setHeight(150);
        aboutStage.setWidth(300);
        
        Parent root = FXMLLoader.load(getClass().getResource("about/About.fxml"));
        
        Scene scene = new Scene(root);
        
        aboutStage.setScene(scene);
        aboutStage.initModality(Modality.APPLICATION_MODAL);
        aboutStage.show();
    }
    
    @FXML
    public void imageViewClicked(MouseEvent e) {
        double x = e.getX();
        double y = e.getY();
        
        Color targetColor = artist.getColor((int) x, (int) y);
        
        if(!(targetColor.equals(Artist.areaHighlightColor) || targetColor.equals(Artist.backgroundColor))) return;
        
        if(chosenVertex > 0) {
            Point oldChosenVertexCoordinates = getVertexPosition(chosenVertex);
            artist.drawSquareFromMemory(oldChosenVertexCoordinates.x, oldChosenVertexCoordinates.y, 2*(highlightedVertexRadius + 1));
            artist.forgetRememberedSquare();
            
            resetVertexHighlightOption();
        }
        
        Color brushColor = targetColor.equals(Artist.areaHighlightColor) ? Artist.backgroundColor : Artist.areaHighlightColor;
        artist.fillArea((int) x, (int) y, brushColor);
    }
    
    @FXML
    public void countChanged(Event e) {
        count = Integer.parseInt(countTextField.getText());
        
        redrawSquares();
    }
    
    @FXML
    public void highlightNumberChanged(Event e) {
        if(chosenVertex > 0) {
            Point oldChosenVertexCoordinates = getVertexPosition(chosenVertex);
            artist.drawSquareFromMemory(oldChosenVertexCoordinates.x, oldChosenVertexCoordinates.y, 2*(highlightedVertexRadius + 1));
            artist.forgetRememberedSquare();
        }
        
        chosenVertex = Integer.parseInt(highlightNumberTextField.getText());
        if(chosenVertex == 0) return;
        
        Point newChosenVertexCoordinates = getVertexPosition(chosenVertex);
        
        artist.rememberSquare(newChosenVertexCoordinates.x, newChosenVertexCoordinates.y, 2*(highlightedVertexRadius + 1));
        
        Color temp = Artist.fillColor;
        Artist.fillColor = Artist.vertexHighlightColor;
        artist.fillCircle(newChosenVertexCoordinates.x, newChosenVertexCoordinates.y, highlightedVertexRadius);
        Artist.fillColor = temp;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        countTextField.textProperty().addListener(countChangeListener);
        highlightNumberTextField.textProperty().addListener(highlightNumberChangeListener);
        
        scaleNewCanvas();
        
        redrawSquares();
    }
    
    private Point getVertexPosition(int vertexNumber) {
        int numberOfLayer = (int)ceil(vertexNumber / (count*4.0));
        
        double step = 90.0 / count;
        
        double helperCos = cos(toRadians(90.0 - 45.0/count));
        double helperSqrt = sqrt(2)/2;
        
        double firstLayerR = helperSqrt * (writableImage.getHeight() / 2);
        
        double layerRadius = firstLayerR * helperSqrt / sin(toRadians(135.0 - (numberOfLayer - 1)*45.0/count));
        
        double centerX = writableImage.getWidth()/2;
        double centerY =  writableImage.getHeight()/2;
        
        double angle = ((count - numberOfLayer) % 2 == 0 ? step/2 : 0) + ((vertexNumber + count*4 - 1) % (count*4)) * step;
        
        return new Point(centerX + layerRadius*cos(toRadians(angle)), centerY + layerRadius*sin(toRadians(angle)));
    }
    
        
    @FXML
    private Label verticeNum;
    @FXML
    private Label sideNum;
    @FXML
    private Label squareNum;
    @FXML
    private Label edgeNum;
    @FXML
    private Label weightNum;

    private void redrawSquares() {
        
            dataManager.calculationSteps(count);
            weightNum.setText(Integer.toString(dataManager.getWeightNumber()));
            verticeNum.setText(Integer.toString(dataManager.getVerticeNumber())); 
            sideNum.setText(Integer.toString(dataManager.getSideNumber()));
            squareNum.setText(Integer.toString(dataManager.getSquareNumber()));
            edgeNum.setText(Integer.toString(dataManager.getEdgeNumber()));
        
        resetVertexHighlightOption();
        
        artist.cleanCanvas();
        artist.forgetRememberedSquare();

        if(count <= 0) return;
        
        double angle = 0;
        double step = 90.0 / count;
        
        while(round(angle * 100000d) / 100000d < 90) {
            artist.drawSquare(writableImage.getWidth()/2, writableImage.getHeight()/2, writableImage.getHeight()/2, angle);
            angle += step;
        }
        
        setGraphVertexes();
    }
    
    private void resetVertexHighlightOption() {
        chosenVertex = 0;
        
        highlightNumberTextField.setText("0");
    }
    
    /*
        R1 = sqtr(2)/2*l   l->side length of square
        Ri = R1*sin(45)/sin(135-(i-1)*45/n)    n->count of squares
        layerVertexDistancei = 2*Ri*cos(90-45/n)
        distance from next layer vertex = Ri - R(i+2)
    */
    private void setGraphVertexes() {
        double step = 90.0 / count;
        
        //to calculate cos(90-45/n) just once
        double helperCos = cos(toRadians(90.0 - 45.0/count));
        //to calculate just once and also for sin(45)
        double helperSqrt = sqrt(2)/2;
        
        double[] layersR = new double[count + 1];
        layersR[1] = helperSqrt * (writableImage.getHeight() / 2);
        if(count > 1)
            layersR[2] = layersR[1] * helperSqrt / sin(toRadians(135.0 - 45.0 / count));
            
        double layerVertexDistance;
        double distanceFromNextLayerVertex;
        double minDistance;
        
        int numberOfLayer = 1;
        do {
            layerVertexDistance = 2 * layersR[numberOfLayer] * helperCos;
            
            if(numberOfLayer + 2 <= count) {
                layersR[numberOfLayer + 2] = layersR[1] * helperSqrt / sin(toRadians(135.0 - (numberOfLayer + 1)*45.0/count));
                distanceFromNextLayerVertex = layersR[numberOfLayer] - layersR[numberOfLayer + 2];
                minDistance = min(layerVertexDistance, distanceFromNextLayerVertex);
            } else
                minDistance = layerVertexDistance;
            
            if(minDistance > minDistanceToDrawVertexes)
                drawLayerVertexes(numberOfLayer, layersR[numberOfLayer]);
            else
                break;
            
            ++numberOfLayer;
        } while(numberOfLayer <= count);
    }
    
    private void drawLayerVertexes(int layerNumber, double layerR) {
        double step = 90.0 / count;
        
        int vertexCountInLayer = 4 * count;
        
        double centerX = writableImage.getWidth()/2;
        double centerY =  writableImage.getHeight()/2;
        
        double angle = (count - layerNumber) % 2 == 0 ? step/2 : 0;
        for(int i = 1; i <= vertexCountInLayer; ++i, angle += step) {
            artist.fillCircle(centerX + layerR*cos(toRadians(angle)), centerY + layerR*sin(toRadians(angle)), vertexRadius);
        }
    }
}
