/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plane.splitting;

/**
 *
 * @author volodyankotsinyan
 */
public class DataManager {
    private int squareNumber;
    private int verticeNumber;
    private int sideNumber;
    private int edgeNumber;
    private int weightNumber;
    
    public void setSquareNumber(int squareNumber) {
        this.squareNumber = squareNumber;
    }
    
    public int getSquareNumber() {
        return this.squareNumber;
    }
    
    public void setVerticeNumber(int verticeNumber) {
        this.verticeNumber = verticeNumber;
    }
    
    public int getVerticeNumber() {
        return this.verticeNumber;
    }
    
    public void setSideNumber(int sideNumber) {
        this.sideNumber = sideNumber;
    }
    
    public int getSideNumber() {
        return this.sideNumber;
    }
    
    public void setEdgeNumber(int edgeNumber) {
        this.edgeNumber = edgeNumber;
    }
    
    public int getEdgeNumber() {
        return this.edgeNumber;
    }
    
    public void setWeightNumber(int weightNumber) {
        this.weightNumber = weightNumber;
    }
    
    public int getWeightNumber() {
        return this.weightNumber;
    }
    
    DataManager(){
        this.edgeNumber = 0;
        this.squareNumber = 0;
        this.sideNumber = 0;
        this.verticeNumber = 0;
        this.weightNumber = 0;
    }
    
    static int combination(int n, int k) {
    if(n==k || n == 1)
        return 1;
    if(k==1)
        return n;
    return combination(n-1, k-1)+combination(n-1, k);
}
    
    public void calculateVertice(int squareNumber) {
        this.verticeNumber = 4 * squareNumber * squareNumber;// + 8 * combination(squareNumber, 2);
    }
    
    public void calculateWeight(int squareNumber) {
        this.weightNumber = 2 * 4 * squareNumber + (squareNumber - 1) * 4 * squareNumber * 4;//8 * combination(squareNumber,2);   
    }
    
    public void calculateEdgeNumber(int weightNumber) {
        this.edgeNumber = weightNumber / 2;
    }
    
    public void calculateSideNumber() {
        this.sideNumber = this.edgeNumber - this.verticeNumber + 2;
    }
    
    public void calculationSteps(int squareNumber) {
        if (squareNumber == 0) {
            this.squareNumber = 0;
            this.edgeNumber = 0;
            this.verticeNumber = 0;
            this.sideNumber = 1;
            this.weightNumber = 0;
            return;
        }
        this.squareNumber = squareNumber;
        this.calculateWeight(squareNumber);
        this.calculateVertice(squareNumber);
        this.calculateEdgeNumber(this.weightNumber);
        this.calculateSideNumber();
    }
    
}
